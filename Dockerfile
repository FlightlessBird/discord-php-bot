FROM php:8.1-cli

RUN apt-get update &&\
    apt install -y zip libzip-dev &&\
    docker-php-ext-configure zip &&\
    docker-php-ext-install zip

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

WORKDIR /var/www
