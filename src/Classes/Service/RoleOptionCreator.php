<?php

declare(strict_types=1);

namespace Bot\Classes\Service;

use Bot\Classes\ConfigLoader;
use Bot\Classes\FileUtils;
use Bot\Classes\Role\RoleCategory;
use Discord\Builders\MessageBuilder;
use Discord\Parts\Channel\Channel;
use Discord\Parts\Channel\Message;

class RoleOptionCreator
{
    public function __construct(
        private ConfigLoader $configLoader,
        private FileUtils $fileUtils
    ){
    }

    public function init(Channel $channel, string $roleConfigPath): void
    {
        $roleCategories = $this->configLoader->loadRoleCategories($roleConfigPath);
        foreach ($roleCategories->getRoleCategories() as $roleCategory) {
            if ($roleCategory->getMessageId() !== '') {
                continue;
            }
            $message = MessageBuilder::new()->setContent($roleCategory->getDescription());
            try {
                $channel->sendMessage($message)->done(function (Message $message) use ($roleCategory, $roleConfigPath) {
                    $this->persistMessageId($message->id, $roleCategory, $roleConfigPath);
                    $this->addReactionsToMessage($message, $roleCategory->getOptions());
                });
            } catch (\RuntimeException $e) {
                var_dump($e);
            }
        }
    }

    private function persistMessageId(string $messageId, RoleCategory $roleCategory, string $roleConfigPath): void
    {
        $roleCategory->setMessageId($messageId);
        $this->fileUtils->writeRoleCategory($roleConfigPath, $roleCategory);
    }

    private function addReactionsToMessage(Message $message, array $reactionOptions): void
    {
        foreach ($reactionOptions as $option) {
            $message->react($option['reaction_id']);
        }
    }
}