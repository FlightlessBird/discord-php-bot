<?php

declare(strict_types=1);

namespace Bot\Classes;

class Config
{
    public static function getToken(): string
    {
        return getenv('TOKEN');
    }

    public static function getRoleConfigPath(): string
    {
        return getenv('ROLE_CONFIG_PATH');
    }

    public static function getRoleChannelName(): string
    {
        return getenv('ROLE_CHANNEL_NAME');
    }
}