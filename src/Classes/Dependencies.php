<?php

declare(strict_types=1);

namespace Bot\Classes;

use Bot\Classes\Service\RoleLocator;
use Bot\Classes\Service\RoleOptionCreator;

class Dependencies
{
    public static function getConfigLoader(): ConfigLoader
    {
        return new ConfigLoader(self::getFileUtils());
    }

    public static function getFileUtils(): FileUtils
    {
        return new FileUtils();
    }

    public static function getRoleOptionCreator(): RoleOptionCreator
    {
        return new RoleOptionCreator(
            self::getConfigLoader(),
            self::getFileUtils()
        );
    }

    public static function getRoleLocator(): RoleLocator
    {
        return new RoleLocator(self::getConfigLoader());
    }
}