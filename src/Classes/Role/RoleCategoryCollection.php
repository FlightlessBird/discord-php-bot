<?php

declare(strict_types=1);

namespace Bot\Classes\Role;


class RoleCategoryCollection
{
    /** @var RoleCategory[] */
    private array $roleCategories;

    private function __construct()
    {
        $this->roleCategories = [];
    }

    public static function create(): self
    {
        return new self();
    }

    public function add(RoleCategory $roleCategory): self
    {
        $this->roleCategories[] = $roleCategory;
        return $this;
    }

    /** @return RoleCategory[] */
    public function getRoleCategories(): array
    {
        return $this->roleCategories;
    }

    public function getCategoryByMessageId(string $messageId): RoleCategory
    {
        foreach ($this->roleCategories as $category) {
            if ($category->getMessageId() === $messageId) {
                return $category;
            }
        }

        throw new \Exception('No message with the id: ' . $messageId . ' found.');
    }
}