<?php

use Bot\Classes\Config;
use Bot\Classes\Dependencies;
use Discord\Discord;
use Discord\Parts\Channel\Channel;
use Discord\Parts\Guild\Guild;

require __DIR__ . '/../vendor/autoload.php';
$discord = new Discord(['token' => Config::getToken()]);

$discord->on('ready', function (Discord $discord) {
    $roleOptionCreator = Dependencies::getRoleOptionCreator();
    /** @var Guild $guild */
    foreach ($discord->guilds as $guild) {
        /** @var Channel $channel */
        foreach ($guild->channels as $channel) {
            if ($channel->name === Config::getRoleChannelName()) {
                $roleOptionCreator->init($channel, Config::getRoleConfigPath() . $guild->name . '/');
                break;
            }
        }
    }
});

$discord->run();